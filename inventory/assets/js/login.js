﻿var Login = function () {
    var simpan = function () {
        //plp.blok();
        var xUserName = $('#NIPP').val();
        var xPassword = $('#Password').val();
        if (xUserName && xPassword) {
            var param = {
                NIPP: xUserName,
                Password: xPassword
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Auth/AuthLogin",
                timeout: 30000
            });
            req.done(function (data) {
                //plp.unblok();
                if (data.sts == 'E') {
                    swal('Error!', 'Error: ' + data.msg, 'error');
                    //$('body').pgNotification({
                    //    style: 'flip',
                    //    message: '<b>Informasi</b><br />' + data.msg,
                    //    position: 'top-right',
                    //    timeout: 2000,
                    //    type: 'error'
                    //}).show();
                } else {
                    window.location.href = data.msg;
                }
            });
        } else {
            //plp.unblok();
            swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
        }
    }

    var etrPassword = function () {
        $('#Password').keypress(function (event) {
            if (event.keyCode === 13) {
                simpan();
            }
        });
    }

    var btnLogin = function () {
        $('#btn-login').click(function () {
            simpan();
        });
    }

    return {
        init: function () {
            btnLogin();
            etrPassword();
        }
    };
}();

$(document).ready(function () {
    Login.init();
});