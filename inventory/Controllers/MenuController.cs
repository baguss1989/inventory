﻿using inventory.DAL;
using inventory.Entities;
using System;
using System.Web.Mvc;

namespace inventory.Controllers
{
    public class MenuController : Controller
    {
        private MenuDAL dal = new MenuDAL();

        // GET: Menu
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getDataMenu()
        {
            //MenuDAL dal = new MenuDAL();
            var result = dal.GetDataMenu();
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddData(APP_MENU data)
        {
            dynamic message = null;
            try
            {
                message = dal.Add(data);
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdData(APP_MENU data)
        {
            dynamic message = null;
            try
            {
                message = dal.Upd(data);
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DelData(APP_MENU data)
        {
            dynamic message = null;
            try
            {
                message = dal.Del(data);
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}