﻿using Dapper;
using inventory.Entities;
using inventory.Helpers;
using System;
using System.Data;
using System.Linq;

namespace inventory.DAL
{
    public class AuthDAL
    {
        public APP_USER GetUserInformation(string username, string password)
        {
            APP_USER result = new APP_USER();
            //wsOuthSoapClient xPortal = new wsOuthSoapClient();
            //UserAkunInfo xAkun = new UserAkunInfo();
            APP_USER_LOKAL getUser = new APP_USER_LOKAL();
            try
            {
                getUser = getUserLokal(username, password);
            }
            catch (Exception e)
            {
                getUser.STS = "E";
            }

            if (getUser.STS == "S")
            {
                result.ID = getUser.NM_USER;
                result.USER_LOGIN = getUser.KD_USER;
                result.USER_ROLE_ID = getUser.ROLE_ID;
                result.KD_CABANG = getUser.KD_CABANG;
                result.KD_TERMINAL = getUser.KD_TERMINAL;
                result.USER_NAME = getUser.NM_USER;
                result.NAMA_CABANG = getUser.NM_CABANG;
                result.NAMA_TERMINAL = getUser.NM_TERMINAL;
                result.USER_ROLE_NAME = getUser.ROLE_NAME;
                result.PROFIT_CENTER = "0";
                result.PERSON_AREA = getUser.PERSON_AREA;
                result.PERSON_SUB_AREA = "0";
                result.PERNR = "0";
                result.LO_ID = "2";
                result.REGIONAL = getUser.REGIONAL;
            }
            //else
            //{
            //    string _KD_TERMINAL = string.Empty;
            //    string _KD_CABANG = string.Empty;
            //    try
            //    {
            //        xAkun = xPortal.valLoginAkun("121", username, password);
            //        if (xAkun.KD_TERMINAL == "01")
            //        {
            //            _KD_TERMINAL = "1";
            //        }
            //        else if (xAkun.KD_TERMINAL == "02")
            //        {
            //            _KD_TERMINAL = "2";
            //        }
            //        else if (xAkun.KD_TERMINAL == "03")
            //        {
            //            _KD_TERMINAL = "3";
            //        }
            //        else if (xAkun.KD_TERMINAL == "04")
            //        {
            //            _KD_TERMINAL = "4";
            //        }
            //        else if (xAkun.KD_TERMINAL == "05")
            //        {
            //            _KD_TERMINAL = "5";
            //        }
            //        else if (xAkun.KD_TERMINAL == "06")
            //        {
            //            _KD_TERMINAL = "6";
            //        }
            //        else if (xAkun.KD_TERMINAL == "07")
            //        {
            //            _KD_TERMINAL = "7";
            //        }
            //        else if (xAkun.KD_TERMINAL == "08")
            //        {
            //            _KD_TERMINAL = "8";
            //        }
            //        else if (xAkun.KD_TERMINAL == "09")
            //        {
            //            _KD_TERMINAL = "9";
            //        }
            //        else
            //        {
            //            _KD_TERMINAL = xAkun.KD_TERMINAL;
            //        }

            //        if (xAkun.KD_CABANG == "01")
            //        {
            //            _KD_CABANG = "1";
            //        }
            //        else if (xAkun.KD_CABANG == "02")
            //        {
            //            _KD_CABANG = "2";
            //        }
            //        else if (xAkun.KD_CABANG == "03")
            //        {
            //            _KD_CABANG = "3";
            //        }
            //        else if (xAkun.KD_CABANG == "04")
            //        {
            //            _KD_CABANG = "4";
            //        }
            //        else if (xAkun.KD_CABANG == "05")
            //        {
            //            _KD_CABANG = "5";
            //        }
            //        else if (xAkun.KD_CABANG == "06")
            //        {
            //            _KD_CABANG = "6";
            //        }
            //        else if (xAkun.KD_CABANG == "07")
            //        {
            //            _KD_CABANG = "7";
            //        }
            //        else if (xAkun.KD_CABANG == "08")
            //        {
            //            _KD_CABANG = "8";
            //        }
            //        else if (xAkun.KD_CABANG == "09")
            //        {
            //            _KD_CABANG = "9";
            //        }
            //        else
            //        {
            //            _KD_CABANG = xAkun.KD_CABANG;
            //        }

            //        if (xAkun.HAKAKSES != "-")
            //        {
            //            result.ID = xAkun.NAMA;
            //            result.USER_LOGIN = xAkun.USERNAME;
            //            result.USER_ROLE_ID = xAkun.HAKAKSES;
            //            result.KD_CABANG = _KD_CABANG;
            //            result.KD_TERMINAL = _KD_TERMINAL;
            //            result.USER_NAME = xAkun.NAMA;
            //            result.NAMA_CABANG = xAkun.NAMA_CABANG;
            //            result.NAMA_TERMINAL = xAkun.NAMA_CABANG;
            //            result.USER_ROLE_NAME = xAkun.HAKAKSES_DESC;
            //            result.PROFIT_CENTER = xAkun.PROFIT_CENTER;
            //            result.PERSON_AREA = xAkun.PERSON_AREA;
            //            result.PERSON_SUB_AREA = xAkun.PERSON_SUB_AREA;
            //            result.PERNR = xAkun.PERNR;
            //            result.LO_ID = xAkun.LO_ID;
            //            result.REGIONAL = xAkun.REGIONAL;
            //            if (_KD_CABANG == "2" && _KD_TERMINAL == "65")
            //            {
            //                result.JNS_DEPO = "PLP";
            //            }
            //            else if (_KD_CABANG == "9" && _KD_TERMINAL == "64")
            //            {
            //                result.JNS_DEPO = "PLP";
            //            }
            //            else
            //            {
            //                result.JNS_DEPO = "MTY";
            //            }
            //        }
            //        else
            //        {
            //            result.USER_ROLE_ID = "E";
            //            result.KD_CABANG = "E";
            //            result.NAMA_CABANG = xAkun.responText;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        result.KD_CABANG = "E";
            //        result.NAMA_CABANG = ex.Message;
            //    }
            //}
            return result;
        }

        private APP_USER_LOKAL getUserLokal(string userName, string passWord)
        {
            APP_USER_LOKAL data = new APP_USER_LOKAL();
            DecEncrypt decrpt = new DecEncrypt();
            string psw = decrpt.Encrypt(passWord);
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var sql = "SELECT KD_CABANG, KD_TERMINAL, KD_USER, NM_USER, PWD_USER, ROLE_ID, TGL_LAHIR, KOTA_LAHIR, " +
                        "ALAMAT, JENIS_KELAMIN, EMAIL, HP, REC_STAT, NM_CABANG, NM_TERMINAL, ROLE_NAME, PERSON_AREA, REGIONAL " +
                        "FROM APP_USER WHERE KD_USER = :KD_USER AND PWD_USER=:PWD_USER";
                    data = connection.Query<APP_USER_LOKAL>(sql, new
                    {
                        KD_USER = userName,
                        PWD_USER = psw
                    }).FirstOrDefault();

                    if (!string.IsNullOrEmpty(data.NM_USER))
                    {
                        if (data.REC_STAT == "D")
                        {
                            data.STS = "E";
                            data.MSG = "USER TERBLOKIR/TIDAK AKTIF";
                        }
                        else
                        {
                            data.STS = "S";
                            data.MSG = "USER VALID";
                        }
                    }
                    else if (string.IsNullOrEmpty(data.NM_USER))
                    {
                        data.STS = "E";
                        data.MSG = "USER ATAU PASSWORD SALAH";
                    }
                    else
                    {
                        data.STS = "E";
                        data.MSG = "USER TERBLOKIR/TIDAK AKTIF";
                    }
                }
                catch (Exception e)
                {
                    data.STS = "E";
                    data.MSG = "USER TIDAK DITEMUKAN";
                }
            }
            return data;
        }
    }
}