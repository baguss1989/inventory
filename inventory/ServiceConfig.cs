﻿using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System.Data;

namespace inventory
{
    public class ServiceConfig
    {
        private static string[] databaseKeys = new[] { "INVENTORY" };

        public static IDbConnection GetConnection()
        {
            IDbConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings[databaseKeys[0]].ConnectionString);
            if (conn.State.Equals(ConnectionState.Closed))
                conn.Open();
            return conn;
        }
    }
}