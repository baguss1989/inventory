﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inventory.Entities
{
    public class APP_USER
    {
        public string ID { get; set; }
        public string USER_LOGIN { get; set; }
        public string USER_ROLE_ID { get; set; }
        public string KD_CABANG { get; set; }
        public string KD_TERMINAL { get; set; }
        public string USER_NAME { get; set; }
        public string NAMA_CABANG { get; set; }
        public string NAMA_TERMINAL { get; set; }
        public string USER_ROLE_NAME { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string PERSON_AREA { get; set; }
        public string PERSON_SUB_AREA { get; set; }
        public string PERNR { get; set; }
        public string LO_ID { get; set; }
        public string REGIONAL { get; set; }
    }

    public class APP_USER_LOKAL
    {
        public string KD_CABANG { get; set; }
        public string KD_TERMINAL { get; set; }
        public string KD_USER { get; set; }
        public string NM_USER { get; set; }
        public string PWD_USER { get; set; }
        public string ROLE_ID { get; set; }
        public DateTime? TGL_LAHIR { get; set; }
        public string KOTA_LAHIR { get; set; }
        public string ALAMAT { get; set; }
        public string JENIS_KELAMIN { get; set; }
        public string EMAIL { get; set; }
        public string HP { get; set; }
        public string REC_STAT { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATED_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string PROGRAM_NAME { get; set; }
        public string NM_CABANG { get; set; }
        public string NM_TERMINAL { get; set; }
        public string ROLE_NAME { get; set; }
        public string PERSON_AREA { get; set; }
        public string REGIONAL { get; set; }
        public string STS { get; set; }
        public string MSG { get; set; }
    }
}